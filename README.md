# Pokemon-Trainer

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://lucaspersson.gitlab.io/pokemon-trainer/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.
CSS was created with [NES.css](https://nostalgic-css.github.io/NES.css/) Version 2.3.0

Pokemon-app with api integration to catch Pokemon's to your profile. 

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project was created for an assignment to learn more about Angular.

## Install

This project uses node, npm and Angular CLI. Go check them out if you don't have them locally installed.

Open a terminal or powershell window and run:

```sh
npm install
```

## Usage

Open a terminal or powershell window and run:

```sh
ng serve
```

Further instructions will appear in your console. Leave the window open while in use.

## Maintainers

[Lucas Persson (@LucasPersson)](https://gitlab.com/LucasPersson)
[Edwin Eliasson (@edwineliasson98)](https://gitlab.com/edwineliasson98)
[Anna Hallberg (@Haruberi)](https://gitlab.com/Haruberi)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Lucas Persson, Edwin Eliasson, Anna Hallberg
