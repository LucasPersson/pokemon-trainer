import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from "src/app/services/user.service";
@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.sass']
})
export class PokemonListComponent implements OnInit {

  @Input() pokemon: Pokemon[] = []

  constructor(
    private userService: UserService

  ) { 
    
  }
  
  ngOnInit(): void {
    
  }
  
    // takes the emit pokemon from child and sends it to add the pokemon to the user 
  sendPokemonToTrainer(pokemon: Pokemon) {
    
    this.userService.addPokemonToTrainer(pokemon.name);
    
  }


}
