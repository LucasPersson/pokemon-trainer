import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.sass']
})
export class PokemonItemComponent implements OnInit {

  // get from parent
  @Input() singlePokemon!: Pokemon;
  @Output() pokemonClick = new EventEmitter<Pokemon>();
  //default value for pokemon is button is clicked catch is = true and pokeball shows
  caught = false 

  

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    // iterate to check if user pokemon = any of the displayed pokemon and add caught to true to display them as caught
    for (const pokemon of this.userService.trainer.pokemon ) {
        if (pokemon === this.singlePokemon.name) {
          this.caught = true
          break;
        }
    }
  }
  // on click that sets caught to true to display caught and then emits pokemon to parent 
  onPokemonClick(pokemon: Pokemon) {
    
    this.caught = true;
    this.pokemonClick.emit(pokemon)
  }


}
