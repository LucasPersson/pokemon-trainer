import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.sass']
})
export class NavComponent implements OnInit {

  get trainer(): Trainer {
    return this.userService.trainer
  }

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }

}
