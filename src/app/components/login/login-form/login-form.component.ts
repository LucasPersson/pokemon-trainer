// Imports
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.sass'],
})
export class LoginFormComponent implements OnInit {

  get loading(): boolean {
    return this.userService.onLoginLoading
  }

  constructor(private userService: UserService) {}

    ngOnInit(): void {
      this.userService.checkLocalStorage();
    }

    // function to submit trainer once created
    onTrainerCreateSubmit(form: NgForm) {

      const { trainer } = form.value;
      
      this.userService.loginTrainer(trainer);
    }
      
  }  
