import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PokemonApiService } from 'src/app/services/pokemon-api.service';

@Component({
  selector: 'app-owned-item',
  templateUrl: './owned-item.component.html',
  styleUrls: ['./owned-item.component.sass']
})
export class OwnedItemComponent implements OnInit {

  // single pokemon from parent
  @Input() pokemon?: string
  @Output() releasePokemonEvent = new EventEmitter<string>()

  // imageUrl
  imageUrl: string = ""

  constructor(
    private pokemonService: PokemonApiService
  ) { }

  ngOnInit(): void {
    // fetch image from pokeAPI and save in imageUrl
    this.pokemonService.getImageURLOfPokemon(this.pokemon || '')
    .subscribe((response) => {      
      this.imageUrl =  response.sprites.front_default
    })
  }

  // emit to parent
  releasePokemon(pokemonName: string) {
    this.releasePokemonEvent.emit(pokemonName)
  }

}
