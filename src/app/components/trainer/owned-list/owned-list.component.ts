import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-owned-list',
  templateUrl: './owned-list.component.html',
  styleUrls: ['./owned-list.component.sass']
})
export class OwnedListComponent implements OnInit {

  // list of pokemon from user parent
  @Input() pokemons?: string[]
  @Output() releasePokemonEvent = new EventEmitter<string>()

  constructor() { }

  ngOnInit(): void {
  }

  // emit to parent
  releasePokemon(pokemon: string) {
    this.releasePokemonEvent.emit(pokemon)
  }

}
