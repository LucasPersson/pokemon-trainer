//Imports
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
} from '@angular/common/http';
import {
  map,
  catchError,
  Observable,
  of,
  tap,
  throwError,
  switchMap,
  finalize,
} from 'rxjs';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { TrainerResponse } from '../models/response.model';

const TRAINER_URL = environment.TrainerAPI;
const TRAINER_KEY = environment.TRAINER_KEY;

@Injectable({
  providedIn: 'root',
})
export class UserService {
  onLoginLoading: boolean = false;
  private _trainer: Trainer = {
    id: 0,
    username: '',
    pokemon: [],
  };

  get trainer(): Trainer {
    return this._trainer;
  }

  constructor(private readonly http: HttpClient, private router: Router) {}

  // loginTrainer - function to log in the trainer
  loginTrainer(username: string) {
    this.onLoginLoading = true
    this.getTrainer(username)
      .pipe(
        switchMap((trainer: Trainer[]) => {
          // check if user exists
          if (trainer.length > 0) {
            return of(trainer[0]);
          }
          // if not post the user and handle in subscribe
          return this.postTrainer(username);
        }),
        finalize(() => this.onLoginLoading = false)
      )
      .subscribe((trainers: Trainer) => {
        this._trainer = trainers;
        localStorage.setItem(TRAINER_KEY, JSON.stringify(this._trainer));
        this.router.navigateByUrl('catalog');
      });
  }

  // logout trainer
  logoutTrainer() {
    // clear local storage
    localStorage.removeItem(TRAINER_KEY)
    // set var to empty username
    this._trainer = {
      id: 0,
      username: '',
      pokemon: []
    }
    // navigate to login
    this.router.navigateByUrl("/")
  }

  // check for the trainer in local storage
  checkLocalStorage() {
    const localStorageUser = localStorage.getItem(TRAINER_KEY);

    if (localStorageUser !== null) {
      this._trainer = JSON.parse(localStorageUser);
      this.router.navigateByUrl('catalog');
    }
  }
  // takes in the click from catch button to add the clicked pokemon to trainers pokemon and then calls function to update api
  addPokemonToTrainer(clickPokemon: string): void {
    let pokemon = clickPokemon;
    this._trainer.pokemon.push(pokemon);
    this.updateTrainerPokemonList().subscribe({});
  }

  // remove pokemon from trainer list and update api and local storage
  releasePokemon(pokemon: string): void {
    this._trainer.pokemon = this.trainer.pokemon.filter(
      (item) => item !== pokemon
    );
    this.updateTrainerPokemonList().subscribe();
  }

  //get function to get trainer from api
  private getTrainer(username: string): Observable<Trainer[]> {
    return this.http.get<Trainer[]>(`${TRAINER_URL}?username=${username}`);
  }

  // post trainers to api
  private postTrainer(username: string): Observable<Trainer> {
    const headers = this.createHeaders()
    const trainer = {
      username,
      pokemon: [],
    };
    return this.http.post<Trainer>(`${TRAINER_URL}`, trainer, { headers });
  }

  // create headers to use in api calls
  private createHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key':
        'EuSLTybkArXa3GDAGJK4czeun0VAQVMb803U0pIHhadhauxw36PwTbT5pKBNl9yv',
    });
  }

  // function that gets called when from addPokemonToTrainer it adds the pokemon in local storage and update the users pokemon in api
  private updateTrainerPokemonList(): Observable<boolean> {
    localStorage.setItem(TRAINER_KEY, JSON.stringify(this._trainer));
    const headers = this.createHeaders();
    return this.http
      .patch<TrainerResponse<Trainer>>(
        `${TRAINER_URL}/${this._trainer.id} `,
        { pokemon: this._trainer.pokemon },
        {
          headers,
        }
      )
      .pipe(
        tap((response) => {
          if (response.success === false) {
            throwError(() => new Error(response.error));
          }
        }),

        map((response: TrainerResponse<Trainer>) => {
          return response.success;
        }),

        catchError((error) => {
          throw error.error.error;
        })
      );
  }
}
