import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { finalize, map, Observable, of } from 'rxjs';
import { PokemonResponse, SinglePokemonResponse } from '../models/response.model';

const URL = environment.pokemonAPI;

@Injectable({
  providedIn: 'root',
})
export class PokemonApiService {
  onScrollIsLoading: boolean = false;
  private _pokemon: Pokemon[] = [];

  get pokemon(): Pokemon[] {
    // getter and the getter also checks to get pokemon from session storage if there is pokemon there. else gets pokemon
    if (sessionStorage.getItem('_pokemon') !== null) {
      this._pokemon = JSON.parse(sessionStorage.getItem('_pokemon') || '');

      return this._pokemon;
    } else {
      
      return this._pokemon;
    }
  }

  constructor(private http: HttpClient) {}
// function that calls api to get all the pokemon limited by 20 at a time
  getAllPokemon(): void {
    this.onScrollIsLoading = true;
    this.http
      .get<PokemonResponse<Pokemon[]>>(
        `${URL}?offset=${this._pokemon.length}&limit=20`
      )
      .pipe(
        map((response: PokemonResponse<Pokemon[]>) => {
          return response.results;
        }),
        finalize(() => {
          this.onScrollIsLoading = false;
        })
      )
      .subscribe({
        next: (pokemon: Pokemon[]) => {
          // iterates over the pokemon to get the id from url and apply the number to sprites and id 
          for (let singlePokemon of pokemon) {
            singlePokemon.id = parseInt(singlePokemon.url.split('/')[6]);
            singlePokemon.imageURL = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
              singlePokemon.url.split('/')[6]
            }.png`;
          }
          // set the pokemon in storage
          this._pokemon = this._pokemon.concat(pokemon);
          sessionStorage.setItem('_pokemon', JSON.stringify(this._pokemon));
        },
      });
  }

  getImageURLOfPokemon(pokemonName: string): Observable<SinglePokemonResponse> {    
    return this.http.get<SinglePokemonResponse>(`${URL}/${pokemonName}`)
  }
}
