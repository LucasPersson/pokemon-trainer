import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { LoginFormComponent } from './components/login/login-form/login-form.component';
import { PokemonListComponent } from './components/catalog/pokemon-list/pokemon-list.component';
import { PokemonItemComponent } from './components/catalog/pokemon-item/pokemon-item.component';
import { OwnedListComponent } from './components/trainer/owned-list/owned-list.component';
import { OwnedItemComponent } from './components/trainer/owned-item/owned-item.component';

import { TrainerPage } from './pages/trainer-page.page';
import { CatalogPage } from './pages/catalog-page.page';
import { LoginPage } from './pages/login-page.page';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginPage,
    CatalogPage,
    TrainerPage,
    LoginFormComponent,
    PokemonListComponent,
    PokemonItemComponent,
    OwnedListComponent,
    OwnedItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
