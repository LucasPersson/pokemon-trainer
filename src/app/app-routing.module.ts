import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './pages/login-page.page';
import { CatalogPage } from './pages/catalog-page.page';
import { TrainerPage } from './pages/trainer-page.page';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginPage },
  { path: 'catalog', component: CatalogPage, canActivate: [AuthGuard] },
  { path: 'trainer', component: TrainerPage, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'login', pathMatch:'full' },
  { path: '**', redirectTo: 'login', pathMatch:'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
