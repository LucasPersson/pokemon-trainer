import { Component } from "@angular/core";
import { Trainer } from "../models/trainer.model";
import { UserService } from "../services/user.service";

@Component({
    selector: 'app-trainer-page',
    templateUrl: './trainer-page.page.html',
    styleUrls: ['./trainer-page.page.sass']
})
export class TrainerPage {

    get trainer(): Trainer {        
        return this.userService.trainer
    }

    constructor (
        private userService: UserService
    ) { }

    // remove pokemon from user
    removePokemon(pokemon: string) {
        this.userService.releasePokemon(pokemon)
    }

    // logout user
    logout() {
        this.userService.logoutTrainer()
    }

}