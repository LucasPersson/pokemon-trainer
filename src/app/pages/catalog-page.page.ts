import { Component, OnInit, HostListener } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { PokemonApiService } from "../services/pokemon-api.service";
import { UserService } from "../services/user.service";

@Component({
    selector: 'app-catalog-page',
    templateUrl: './catalog-page.page.html'
})
export class CatalogPage implements OnInit {

   //get pokemon
    get pokemon(): Pokemon[] {
        return this.pokemonService.pokemon;
    }

    //on scroll event to trigger the loading infinite scroll by calling api on bottom of page on catalog page 
    @HostListener('window:scroll', ['$event'])
    onScroll($event: Event): void {
        
    if ((window.innerHeight + window.scrollY + 10) >= document.body.offsetHeight) {
      if (this.pokemonService.onScrollIsLoading === false) {
          this.pokemonService.getAllPokemon();
      } 
    }
  }

    
    constructor (
        private userService: UserService,
        private pokemonService: PokemonApiService,

    ) { }

    ngOnInit(): void {
        // check to se if its the first time loading the catalog page
        if (sessionStorage.getItem('_pokemon') === null) {
          this.pokemonService.getAllPokemon();
      }
    }
    

}