export interface PokemonResponse<T> {
    results: T;
    success: boolean;
    error?: string;
}

export interface TrainerResponse<T> {
    id: number,
    username: string,
    pokemon: string[],
    success: boolean;
    error?: string;
}

export interface SinglePokemonResponse {
    sprites: Sprites
}

export interface Sprites {
    front_default: string
}