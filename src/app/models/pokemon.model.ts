export interface Pokemon {
    id: number,
    name: string,
    url: string,
    imageURL: string
}