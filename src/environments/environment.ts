// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  pokemonAPI: 'https://pokeapi.co/api/v2/pokemon',
  TrainerAPI: 'https://avokad-pingvin-api.herokuapp.com/trainers',
  TRAINER_KEY: 'pokemon-trainer',
  pokemonGif: '/assets/pikachu-hi.gif.gif'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
