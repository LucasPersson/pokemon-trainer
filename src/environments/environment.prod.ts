export const environment = {
  production: true,
  pokemonAPI: 'https://pokeapi.co/api/v2/pokemon',
  TrainerAPI: 'https://avokad-pingvin-api.herokuapp.com/trainers',
  TRAINER_KEY: 'pokemon-trainer',
  pokemonGif: '/pokemon-trainer/assets/pikachu-hi.gif.gif'
};
